package com.epam.generics.typesystem.wildcardsbounds.enums;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;

import org.junit.Test;

import com.epam.generics.typesystem.wildcardsbounds.enums.Department;

// Dissecting an enum. Also a small demo of
// reflection for generics.
public class DepartmentTest {

	@Test
	public void test() {
		Type superclass = Department.class.getGenericSuperclass();
		ParameterizedType pType = (ParameterizedType) superclass;
		System.out.println("type: " + pType.getRawType());
		System.out.println("type params: " + Arrays.toString(pType.getActualTypeArguments()));
		System.out.println(superclass);
	}

}
