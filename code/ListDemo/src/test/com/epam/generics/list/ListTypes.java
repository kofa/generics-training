package com.epam.generics.list;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Test;

// This class has compilation errors on purpose,
// to demonstrate type safety. It compares what
// safety is provided by raw types (none),
// those parameterised with Object, and those
// parameterised with ? (QMark).
public class ListTypes {
	@Test
	public void testRaw() {
		List list = new ArrayList();

		list.add("hello");
		list.add(new Date());

		String hello = (String) list.get(0);
		Date now = (Date) list.get(1);
		qMarkListMethod(list);
		objectListMethod(list);
		// danger: List holds unspecified objects, NOT Strings!
		stringListMethod(list);
	}

	@Test
	public void testObject() {
		List<Object> list = new ArrayList<>();

		list.add("hello");
		list.add(new Date());

		String hello = (String) list.get(0);
		Date now = (Date) list.get(1);
		qMarkListMethod(list);
		objectListMethod(list);
		// this compilation error is expected - List holds Objects, not Strings
		stringListMethod(list);
	}

	@Test
	public void testQMark() {
		qMarkListMethod(Arrays.asList("hello", "world"));
		List<?> list = new ArrayList<String>();

		// These errors are expected - it'd be incorrect to add items to a List
		// of unknown type
		list.add("hello");
		list.add(new Date());

		// null is compatible with all types
		list.add(null);

		String hello = (String) list.get(0);
		Date now = (Date) list.get(1);
		qMarkListMethod(list);

		// These compilation errors are expected, the List holds an unknown
		// type, not Objects or Strings
		objectListMethod(list);
		stringListMethod(list);
	}

	public void qMarkListMethod(List<?> param) {
		System.out.println("?" + param);
	}

	public void objectListMethod(List<Object> param) {
		System.out.println("Object" + param);
	}

	public void stringListMethod(List<String> param) {
		System.out.println("String" + param);
	}

}
