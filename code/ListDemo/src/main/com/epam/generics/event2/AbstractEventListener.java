package com.epam.generics.event2;

public abstract class AbstractEventListener<E extends Event> implements
		EventListener<E> {
	private final Class<E> eventType;

	protected AbstractEventListener(Class<E> eventType) {
		this.eventType = eventType;
	}

	@Override
	public Class<E> getEventType() {
		return eventType;
	}
}
