package com.epam.generics.event2;

public class EventTwoListener extends AbstractEventListener<EventTwo> {

	protected EventTwoListener() {
		super(EventTwo.class);
	}

	@Override
	public void onEvent(EventTwo event) {
		System.out.println("Hello, happily handing type-safe EventTwo:"	+ event);
	}

}
