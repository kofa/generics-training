package com.epam.generics.event2;

public class EventOneListener extends AbstractEventListener<EventOne> {

	protected EventOneListener() {
		super(EventOne.class);
	}

	@Override
	public void onEvent(EventOne event) {
		System.out.println("Hello, happily handing type-safe EventOne:"	+ event);
	}

}
