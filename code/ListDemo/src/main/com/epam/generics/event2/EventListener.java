package com.epam.generics.event2;

public interface EventListener<E extends Event> {
	// In this version, casting to required type
	// is done in EventDispatcher. This way is
	// a bit more type-safe, but EventDispatcher
	// needs to extract type from EventListener
	// to perform the cast. See code smell
	// 'feature envy'.
	void onEvent(E e);
	Class<E> getEventType();
}
