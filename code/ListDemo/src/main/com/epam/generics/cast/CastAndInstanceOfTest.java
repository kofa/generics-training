package com.epam.generics.cast;

import static org.junit.Assert.*;

import org.junit.Test;

// Demonstrates different ways to cast / check compatibility.
// Casts like '(Foo) bar' are checked at run-time, but have to
// be defined statically, when the program is compiled, and
// may generate warnings even if the code is correct.
// The same need to define everything statically for
// 'foo instanceof Bar' checks. The class Class provides methods
// to do all of that dynamically: see cast(), isInstance(),
// isAssignableFrom().
public class CastAndInstanceOfTest {
	@Test
	public void testInstanceCheck() {
		// static
		assertTrue("hello" instanceof String);
		// dynamic - in checkType, the class to check is a parameter,
		// can vary at run-time
		assertTrue(checkType(String.class, "hello"));
	}
	
	@Test
	public void testSubTypeCheck() {
		// compares types, not an instance against a type
		assertTrue(Number.class.isAssignableFrom(Integer.class));
		assertFalse(Integer.class.isAssignableFrom(Number.class));
	}
	
	@Test
	public void testCast() {
		Object hello ="hello";
		// static
		String greeting = (String) hello;
		// dynamic - in cast, the class to cast into is a parameter,
		// can vary at run-time
		String hi = cast(String.class, hello);
	}
	
	private boolean checkType(Class<?> type, Object o) {
		return type.isInstance(o);
	}
	
	private <T> T cast(Class<T> type, Object o) {
		return type.cast(o);
	}
}
