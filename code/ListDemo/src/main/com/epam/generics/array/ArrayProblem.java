package com.epam.generics.array;

public class ArrayProblem {
	public static void main(String[] args) {
		Number[] numbers;
		Integer[] ints = { 1, 2, 3 };
		numbers = ints;
		numbers[0] = Math.PI;
		for (Number n : numbers) {
			System.out.println(n);
		}
		for (Integer i : ints) {
			System.out.println(i);
		}
	}
}
