package com.epam.generics.array;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class Sum {
	public double sum(Number[] numbers) {
		double sum = 0;
		for (Number n : numbers) {
			sum += n.doubleValue();
		}
		return sum;
	}
	
	public double sum(List numbers) {
		double sum = 0;
		for (Object n : numbers) {
			sum += ((Number) n).doubleValue();
		}
		return sum;
	}

	@Test
	public void testArrayWithNumbers() {
		Number[] numbers = new Number[] {1, 2.0f, 3.0d, 4L};
		double sum = sum(numbers);
		assertEquals(10.0, sum, 10E-4);
	}
	

	@Test
	public void testArrayWithIntegers() {
		Integer[] numbers = new Integer[] {1, 2, 3, 4};
		double sum = sum(numbers);
		assertEquals(10.0, sum, 10E-4);
	}
	
	@Test
	public void testListWithNumbers() {
		List numbers = new ArrayList();
		numbers.add(1);
		numbers.add(2.0f);
		numbers.add(3.0d);
		numbers.add(4L);
		double sum = sum(numbers);
		Assert.assertEquals(10.0, sum, 10E-4);
	}

	@Test
	public void testListWithIntegers() {
		List numbers = new ArrayList();
		numbers.add(1);
		numbers.add(2);
		numbers.add(3);
		numbers.add(4);
		double sum = sum(numbers);
		Assert.assertEquals(10.0, sum, 10E-4);
	}
}
