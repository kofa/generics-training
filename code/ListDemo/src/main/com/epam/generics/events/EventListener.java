package com.epam.generics.events;

public interface EventListener<E extends Event> {
	void handleEvent(E event);

	Class<E> getHandledType();

	void handleEventSimple(Event event);

}
