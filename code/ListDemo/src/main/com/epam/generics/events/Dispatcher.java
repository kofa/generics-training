package com.epam.generics.events;

import java.util.HashMap;
import java.util.Map;

public class Dispatcher {
	private final Map<Class<? extends Event>, EventListener<? extends Event>> listeners = new HashMap<>();

	public <E extends Event> void setListener(Class<E> type,
			EventListener<E> listener) {
		listeners.put(type, listener);
	}

	public void dispatchEvent(Event e) {
		Class<? extends Event> type = e.getClass();
		EventListener<? extends Event> eventListener = listeners.get(type);
		doDispatch(e, eventListener);
		// eventListener.handleEventSimple(e);
	}

	private <E extends Event> void doDispatch(Event event,
			EventListener<E> listener) {
		E typeSafeEvent = listener.getHandledType().cast(event);
		listener.handleEvent(typeSafeEvent);
	}
}
