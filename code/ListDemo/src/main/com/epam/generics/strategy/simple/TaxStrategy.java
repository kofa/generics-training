package com.epam.generics.strategy.simple;

public interface TaxStrategy {
	long calculateTax(TaxPayer p);
}
