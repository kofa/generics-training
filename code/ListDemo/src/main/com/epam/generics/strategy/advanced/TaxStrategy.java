package com.epam.generics.strategy.advanced;

public interface TaxStrategy {
	long calculateTax(TaxPayer p);
}
