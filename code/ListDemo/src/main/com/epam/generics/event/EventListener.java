package com.epam.generics.event;

public interface EventListener<E extends Event> {
	// In this version, casting to required type
	// is done in AbstractEventListener
	// and actual event handling is performed
	// in the protected handleEvent(E) method
	// implemented by concrete subclasses.
	void onEvent(Event e);
	
	Class<E> getEventType();
}
