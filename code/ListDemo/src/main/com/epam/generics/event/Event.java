package com.epam.generics.event;

public abstract class Event {
	private final String name;

	protected Event(String name) {
		this.name = name;

	}

	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + "/" + getName();
	}
}
