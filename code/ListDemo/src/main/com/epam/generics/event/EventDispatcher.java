package com.epam.generics.event;

import java.util.HashMap;
import java.util.Map;

public class EventDispatcher {
	private final Map<Class<? extends Event>, EventListener<? extends Event>> listeners = new HashMap<>();

	public <E extends Event> void register(EventListener<E> listener) {
		listeners.put(listener.getEventType(), listener);
	}

	public void dispatch(Event e) {
		EventListener<? extends Event> listener = listeners.get(e.getClass());
		if (listener == null) {
			throw new RuntimeException("No listener for " + e.getClass());
		}
		notify(listener, e);
	}
	
	private <E extends Event> void notify(EventListener<E> listener, Event event) {
		E typeSafeEvent = listener.getEventType().cast(event);
		listener.onEvent(typeSafeEvent);
	}
}
