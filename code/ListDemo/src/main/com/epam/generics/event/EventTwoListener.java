package com.epam.generics.event;

public class EventTwoListener extends AbstractEventListener<EventTwo> {

	protected EventTwoListener() {
		super(EventTwo.class);
	}

	@Override
	protected void handleEvent(EventTwo event) {
		System.out.println("Hello, happily handing type-safe EventTwo:"	+ event);
	}

}
