package com.epam.generics.event;

public abstract class AbstractEventListener<E extends Event> implements
		EventListener<E> {
	private final Class<E> eventType;

	protected AbstractEventListener(Class<E> eventType) {
		this.eventType = eventType;
	}

	@Override
	public Class<E> getEventType() {
		return eventType;
	}
	
	@Override
	public void onEvent(Event e) {
		E typeSafeEvent = getEventType().cast(e);
		handleEvent(typeSafeEvent);
	}

	protected abstract void handleEvent(E typeSafeEvent);
}
