package com.epam.generics.event;

public class EventOneListener extends AbstractEventListener<EventOne> {

	protected EventOneListener() {
		super(EventOne.class);
	}

	@Override
	protected void handleEvent(EventOne event) {
		System.out.println("Hello, happily handing type-safe EventOne:"	+ event);
	}

}
